import numpy as np
import matplotlib.pyplot as plt

from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn.datasets.samples_generator import make_blobs
from sklearn.preprocessing import StandardScaler
from scipy import optimize


def plot_clusters(X, labels, n_clusters_):
    # Black removed and is used for noise instead.
    unique_labels = set(labels)
    colors = plt.cm.Spectral(np.linspace(0, 1, len(unique_labels)))
    for k, col in zip(unique_labels, colors):
        if k == -1:
            # Black used for noise.
            col = 'k'

        class_member_mask = (labels == k)

        xy = X[class_member_mask & core_samples_mask]
        plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=col,
                 markeredgecolor='k', markersize=14)

        xy = X[class_member_mask & ~core_samples_mask]
        plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=col,
                 markeredgecolor='k', markersize=6)

    plt.title('Estimated number of clusters: %d' % n_clusters_)
    plt.show()


def jaccard_dist(v1, v2):
    v1_shifted = v1 - np.min(v1)
    v2_shifted = v2 - np.min(v2)
    return 1.0 - np.sum(np.min([v1_shifted, v2_shifted], axis=0)) / np.sum(np.max([v1_shifted, v2_shifted], axis=0))


def cosine_dist(v1, v2):
    # 1 - (cos_sim(v1,v2) + 1) / 2, as cos_sim ranges [-1,1], and we need reversed (distance = dissimilarity) [0,1]
    cos_sim = np.sum(np.multiply(v1, v2)) / (np.sqrt(np.sum(np.multiply(v1, v1))) * np.sqrt(np.sum(np.multiply(v2, v2))))
    return 1.0 - (cos_sim + 1) / 2.0


def lpnorm_dist(v1, v2, p):
    return np.sum(np.power(np.abs(v1 - v2), np.repeat(p, len(v1))))


def dbscan_cluster_cost_fun(eps, *args):
    if eps < 0 or eps > 1:  # eps can only be in the range (0,1)
        return 1
    X, min_samples, metric, D = args
    db_in = X
    if metric == 'precomputed':
        db_in = D
    db = DBSCAN(eps=eps[0], min_samples=min_samples, metric=metric).fit(db_in)
    if len(set(db.labels_)) < 2:  # at least 2 cluster is needed
        return 1

    # returns [0,1], where 0 is the best, 1 is the worst
    # silhouette outputs [-1,1] -> [0,1]
    return 1 - (metrics.silhouette_score(X, db.labels_) + 1) / 2.0


# dummy input
centers = [[1, 1], [-1, -1], [1, -1]]
X, labels_true = make_blobs(n_samples=750, centers=centers, cluster_std=0.4, random_state=0)
X = StandardScaler().fit_transform(X)


# precompute distance mx
def precompute_dist_mx(X, d_metric, *args):
    D = np.zeros((len(X), len(X)))
    for i1, x1 in enumerate(X):
        for i2, x2 in enumerate(X):
            D[i1, i2] = d_metric(x1, x2, *args)
    return D


D = precompute_dist_mx(X, jaccard_dist)  # here choose the distance metric

# find best eps value by simulated annealing
min_samples = 10  # fixed
metric = 'precomputed'  # 'precomputed' or function like f(v1,v2)
params = {'args': (X, min_samples, metric, D)}
x0 = np.array(0.2)
res = optimize.basinhopping(dbscan_cluster_cost_fun, x0=x0, minimizer_kwargs=params, disp=True)
print("global minimum: x = %.4f, f(x0) = %.4f, silhouette: %.4f" % (res.x, res.fun, 2.0 * (1.0 - res.fun) - 1.0))
opt_eps = res.x

print 'opt: ' + str(opt_eps)

# compute dbscan using the optimal eps
db_in = X
if metric == 'precomputed':
    db_in = D
db = DBSCAN(eps=opt_eps, min_samples=min_samples, metric=metric).fit(db_in)
core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
core_samples_mask[db.core_sample_indices_] = True
labels = db.labels_

# number of clusters in labels, ignoring noise if present
n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

print('Estimated number of clusters: %d' % n_clusters_)
print("Homogeneity: %0.3f" % metrics.homogeneity_score(labels_true, labels))
print("Completeness: %0.3f" % metrics.completeness_score(labels_true, labels))
print("V-measure: %0.3f" % metrics.v_measure_score(labels_true, labels))
print("Adjusted Rand Index: %0.3f" % metrics.adjusted_rand_score(labels_true, labels))
print("Adjusted Mutual Information: %0.3f" % metrics.adjusted_mutual_info_score(labels_true, labels))
print("Silhouette Coefficient: %0.3f" % metrics.silhouette_score(X, labels))

plot_clusters(X, labels, n_clusters_)