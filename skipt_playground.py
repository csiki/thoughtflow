
import skip_thoughts.skipthoughts as skipt
import numpy as np

model = skipt.load_model()

t1 = [u"I love Mary!"]
t2 = [u"I hate Mary!"]

v1, success = skipt.encode(model, t1, use_eos=True)
v2, success = skipt.encode(model, t2, use_eos=True)

print np.max(v1), np.min(v1), np.max(v2), np.min(v2)

