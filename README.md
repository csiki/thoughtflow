How to run:

launch flask app: python run.py
launch a worker: celery -A app.celery_worker.celery worker --loglevel=info
launch a scheduler: celery -A app.celery_worker.celery beat --loglevel=info

to load skip thoughts library:
mkdir skipt_models
cd skipt_models
wget http://www.cs.toronto.edu/~rkiros/models/dictionary.txt
wget http://www.cs.toronto.edu/~rkiros/models/utable.npy
wget http://www.cs.toronto.edu/~rkiros/models/btable.npy
wget http://www.cs.toronto.edu/~rkiros/models/uni_skip.npz
wget http://www.cs.toronto.edu/~rkiros/models/uni_skip.npz.pkl
wget http://www.cs.toronto.edu/~rkiros/models/bi_skip.npz
wget http://www.cs.toronto.edu/~rkiros/models/bi_skip.npz.pkl

install rabbitmq for celery:
sudo apt-get install rabbitmq-server

download nltk resources:
python
>> import nltk
>> nltk.download('all', download_dir='/mnt/disks/data/nltk')  # or whatever the download dir should be

change nltk added path if needed in skipthoughts.py, skip_thought/.../tools.py, eval_msrp.py

python -m spacy.en.download all

run glove_to_skipt.py to generate g2s model
