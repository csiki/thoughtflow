import random
import sqlite3
import cPickle as pickle
from os.path import isfile
from pprint import pformat
import numpy as np
from flask import Flask, render_template, redirect, session, escape, g, request
import config
from collections import OrderedDict
import json
from lib import sql_commands
from datetime import datetime
import time
import spacy
import re
import tensorflow as tf

# to import module from parent directory
import os
import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
os.sys.path.insert(0, parentdir)
import skip_thoughts.skipthoughts as skipt
from lib import adjust_thought

# flask server
app = Flask(__name__)

# models
model = skipt.load_model()
app.secret_key = config.SECRET_KEY


# TODO store (most) sql commands in a separate file
# TODO usage? http://scikit-learn.org/stable/modules/generated/sklearn.metrics.silhouette_samples.html#sklearn.metrics.silhouette_samples
# TODO and http://stats.stackexchange.com/questions/79028/performance-metrics-to-evaluate-unsupervised-learning
# TODO make a fake skipt model - a smaller one so this shitface app can be tested in human lifetime - just create fake dictionary files for skipt
# TODO dataflow integration

# TODO solve https
# TODO script to print all thoughts and their groupings to command line - admin script
# TODO present random topics on the main page, change it time to time, until enough users
# TODO UI for voting


@app.route('/')
def index():
    if 'user' not in session:  # TODO need captcha
        session['user'] = '%032x' % random.getrandbits(128)  # TODO have proper accounts/users

    # get all sessions corresponding to the user
    conn = get_db()
    cursor = conn.cursor()
    cursor.execute("SELECT SESSION, SENTENCE, CLUSTERED FROM THOUGHTS WHERE USER = ?", (session['user'],))
    thoughts = cursor.fetchall()
    thoughts_info = []
    for t in thoughts:
        # clustered: if 0, present clustering, if -1, still clustering, if 1, group page
        t_info = {}
        if t[2] == 0:
            t_info['state'] = 'not yet clustered'
        elif t[2] == 1:
            t_info['state'] = 'in group'
        else:
            t_info['state'] = 'out of group'
        t_info['path'] = '/switch?session=%s' % t[0]
        t_info['sentence'] = t[1]
        thoughts_info.append(t_info)

    return render_template('input.html', thoughts=thoughts_info)


@app.route('/switch', methods=['GET'])
def switch_to_session():
    path = '/'  # default path to redirect to

    if 'session' in request.args:
        # check if session actually corresponds to the given user
        conn = get_db()
        cursor = conn.cursor()
        cursor.execute("SELECT CLUSTERED FROM THOUGHTS WHERE USER = ? AND SESSION = ?",
                       (session['user'], request.args['session']))
        clustered = cursor.fetchone()
        if clustered is not None:
            session['session'] = request.args['session']
            if clustered[0] == 0:
                path = '/clustering'
            elif clustered[0] == 1:
                path = '/group'
            elif clustered[0] == -1:
                path = '/retry-clustering'

    return redirect(path, code=302)


@app.route('/syn', methods=['GET', 'POST'])
def syn():
    # FIXME and 'user' in session\ instead of accepting user from request.form or .args
    if request.method == 'POST' and 'thought' in request.form and\
            ('user' in session or 'user' in request.form or 'user' in request.args):
        user = session.get('user', request.form.get('user', request.args.get('user')))
        thought = request.form['thought']

        # handle thought input  # TODO move to separate flask method
        if len(thought) > 0 and len(thought.split()) > 1:

            # TODO could use separate process that encode multiple thoughts at a time in batches, later
            # TODO so loading wouldn't take ages

            # remove end of sentence character
            thought = thought.strip()
            if thought[-1] in ('.', '?', '!'):
                thought = thought[:-1]

            glove = spacy.load('en')  # load spacy word representation as vocabulary expansion to skipt
            glove_vocab = {v.orth_: v.vector for v in glove.vocab}  # build vocab lookup table
            encoded, meta_info = skipt.encode(model, [thought], glove_vocab, verbose=False, use_eos=True)
            # adjusted = adjust_thought.adjust(encoded)
            # adjusted = np.array([1,2,3,4,5]*960)

            # if found an unknown word, add it to database and send user back to index page
            conn = get_db()
            if len(meta_info['unknown_words']) > 0:
                pattern = re.compile("[A-Za-z'-]+")
                for unk_w in meta_info['unknown_words']:
                    unk_w_norm = unk_w.strip().lower()
                    if pattern.match(unk_w_norm):
                        conn.execute("INSERT OR IGNORE INTO UNKNOWN_WORDS (WORD) VALUES (?)", (unk_w_norm,))
                conn.commit()

                # user message
                singular_msg = 'Try to rephrase your sentence! The unrecognized word is: %s. '\
                               'Nevertheless, this word is added to the database and is automatically learnt later on.'
                plural_msg = 'Try to rephrase your sentence! The unrecognized words are: %s. '\
                             'Nevertheless, these words are added to the database and are automatically learnt later on.'\

                message = (singular_msg if len(meta_info['unknown_words']) == 1 else plural_msg) %\
                          ', '.join(meta_info['unknown_words'])
                return render_template('input.html', thought=thought, message=message)

            # generate session id
            session_id = '%032x' % random.getrandbits(128)  # 128 bits = 32 hex
            session['session'] = session_id  # thought session

            # store 'pending' thought in db
            conn.execute("INSERT INTO THOUGHTS (SESSION, USER, SENTENCE, ENCODED, CLUSTERED) VALUES(?, ?, ?, ?, ?)",
                         (session_id, user, thought, pickle.dumps(encoded), 0))
            conn.commit()

            return redirect('/clustering', 302)
        else:
            return render_template('input.html', thought=thought, message='Invalid thought.. :D')

    return redirect('/', code=302)  # back to index


@app.route('/clustering/state')
def is_clustered():
    response = {}

    if 'user' in session and 'session' in session:
        conn = get_db()
        cursor = conn.cursor()

        # validate user-session pair
        cursor.execute('SELECT CLUSTERED FROM THOUGHTS WHERE SESSION = ? AND USER = ?', (session['session'], session['user']))
        clustered = cursor.fetchone()
        if clustered is not None:
            # get latest clustering id
            clustered = clustered[0]
            clustering_id = sql_commands.latest_clustering_id(conn)
            response['clustering_id'] = clustering_id
            response['clustered'] = clustered
            if clustered == 1:
                # find group id corresponding to the user/session and latest cluster id
                cursor.execute("SELECT GROUPING.GROUP_ID FROM GROUPING "
                               "WHERE GROUPING.SESSION = ? AND CLUSTERING_ID = ?", (session['session'], clustering_id))
                group_id = cursor.fetchone()
                if group_id is not None:
                    response['group'] = group_id[0]

    return json.dumps(response)


@app.route('/clustering')
def loading():
    if 'user' not in session or 'session' not in session:
        return redirect('/', code=302)

    # check changes in THOUGHTS table, CLUSTERED column
    conn = get_db()
    cursor = conn.cursor()
    cursor.execute("SELECT CLUSTERED FROM THOUGHTS WHERE USER = ? AND SESSION = ?",
                   [session['user'], session['session']])

    clustered = cursor.fetchone()[0]
    if clustered == 1:  # fetch the corresponding group and redirect there
        cursor.execute("SELECT GROUP_ID FROM GROUPING WHERE SESSION = ? ORDER BY CLUSTERING_ID DESC",
                       [session['session']])
        group_id = cursor.fetchone()[0]
        return redirect('/group?group=%s' %
                        (escape(group_id)), code=302)
    elif clustered == -1:  # redirect to retry clustering
        return redirect('/retry-clustering', code=302)

    # else show loading page
    return render_template('loading.html')


@app.route('/retry-clustering')
def still_loading():
    if 'user' not in session or 'session' not in session:
        return redirect('/', code=302)

    # check changes in THOUGHTS table, CLUSTERED column
    conn = get_db()
    cursor = conn.cursor()
    cursor.execute("SELECT CLUSTERED FROM THOUGHTS WHERE USER = ? AND SESSION = ?",
                   (session['user'], session['session']))

    clustered = cursor.fetchone()[0]
    if clustered == 1:  # fetch the corresponding group and redirect there
        cursor.execute("SELECT GROUP_ID FROM GROUPING WHERE SESSION = ? ORDER BY CLUSTERING_ID DESC",
                       [session['session']])
        group_id = cursor.fetchone()[0]  # fetch the first that has the highest clustering id = latest
        return redirect('/group?group=%s&user=%s&session=%s' %
                        (escape(group_id), escape(session['user']), escape(session['session'])), code=302)

    # else search for alternative groups
    # TODO search in groups table - measure the distance between the thought and all the groups

    return render_template('still_loading.html')  # like the loading page but w/ options to join nearby groups


@app.route('/group', methods=['GET', 'POST'])
def group_interface():
    if ('user' not in session or 'session' not in session) and 'group' not in request.args:
        return redirect('/', code=302)

    conn = get_db()
    cursor = conn.cursor()
    clustering_id = sql_commands.latest_clustering_id(conn)

    # if user activity received, validate and upload to db  # TODO move to separate flask function
    message = None
    added_post = None
    added_vote = None
    if 'user' in session and 'session' in session and\
            (('post' in request.form and 'parent' in request.form and len(request.form['post']) < config.MAX_POST_LEN
             and (request.form['parent'].isdigit() or request.form['parent'] == '-1' or request.form['parent'] == '')) ^  # post
                ('vote' in request.form and 'voted' in request.form and request.form['vote'] in ('1', '-1'))):  # vote
        if 'post' in request.form:
            # check for rapid interaction
            cursor.execute("SELECT TS FROM POSTS WHERE SESSION = ? AND TS + ? > strftime('%s', 'now') ORDER BY TS DESC",
                           (session['session'], config.MIN_TIME_BETWEEN_POSTS))
            rapid_interaction = cursor.fetchall()
            if len(rapid_interaction) > 0:
                message = 'too frequent interaction!'
            else:
                # check for invalid parent
                parent = -1 if request.form['parent'] == '' else int(request.form['parent'])
                parent_existence = False
                if parent != -1:  # not root
                    cursor.execute("SELECT ID FROM POSTS WHERE ID = ?", (parent,))
                    parent_existence = cursor.fetchone()
                if parent == -1 or parent_existence is not None:
                    cursor.execute("INSERT OR IGNORE INTO POSTS (SESSION, PARENT, TEXT) VALUES (?, ?, ?)",
                                   (session['session'], parent, escape(request.form['post'].strip())))
                    added_post = cursor.lastrowid
                    sql_commands.revive_thought(conn, session['session'])
        else:  # 'vote' is in request.form
            # check for rapid interaction
            cursor.execute("SELECT TS FROM VOTES WHERE SESSION = ? AND TS + ? > strftime('%s', 'now') ORDER BY TS DESC",
                           (session['session'], config.MIN_TIME_BETWEEN_VOTES))
            rapid_interaction = cursor.fetchall()
            if len(rapid_interaction) > 0:
                message = 'too frequent interaction!'
            else:
                # check for invalid post id
                post_id = int(request.form['voted'])
                voted = int(request.form['vote'])
                cursor.execute("SELECT ID FROM POSTS WHERE ID = ?", (post_id,))
                if len(cursor.fetchall()) > 0:
                    # check if already voted
                    cursor.execute("SELECT SESSION FROM VOTES WHERE SESSION = ? AND POST_ID = ?",
                                   (session['session'], post_id))
                    if len(cursor.fetchall()) == 0:
                        cursor.execute("INSERT INTO VOTES (SESSION, POST_ID, SIGN) VALUES (?, ?, ?)",
                                       (session['session'], post_id, voted))
                        added_vote = post_id
                        sql_commands.revive_thought(conn, session['session'])
                    else:
                        message = "already voted on post # %d" % post_id
                else:
                    message = "invalid post id number"

    # lookout, early commit
    conn.commit()

    # determine write rights and group to show
    write_rights = False
    chosen_group = request.args.get('group', None)
    if 'user' in session and 'session' in session:
        # check if user-session exists in grouping with the last clustering id
        # if it does, check if group is given, if yes does it correspond to the one in db
        # if yes write_rights given, if not write_rights is not given
        # also if no group is given it isselected as found in db - if not found redirect to main page
        cursor.execute("SELECT GROUP_ID FROM GROUPING WHERE SESSION = ? AND CLUSTERING_ID = ?",
                       (session['session'], clustering_id))
        actual_group = cursor.fetchone()

        if chosen_group is not None and actual_group is not None and chosen_group == actual_group[0]:
            write_rights = True
        elif chosen_group is None and actual_group is not None:
            chosen_group = actual_group[0]
            write_rights = True

    # if chosen_group is still None = no group is chosen and there is no actual group corresponding to user data
    if chosen_group is None:
        return redirect('/', code=302)
    else:  # check if group exists, redirect if not
        cursor.execute("SELECT ID FROM GROUPS WHERE ID = ?", (chosen_group,))

    # gather sessions that are in the group
    cursor.execute('SELECT SESSION FROM GROUPING WHERE CLUSTERING_ID = ? AND GROUP_ID = ? AND ACCEPTANCE >= 0',
                   (clustering_id, chosen_group))
    sessions = cursor.fetchall()

    # gather posts/comments from sessions that are in the group
    cursor.execute('''SELECT sessions.session, sessions.user, posts.ID, posts.PARENT, posts.TEXT, posts.TS FROM
                        (SELECT GROUPING.SESSION AS session, THOUGHTS.USER AS user FROM GROUPING, THOUGHTS
                        WHERE CLUSTERING_ID = ? AND GROUP_ID = ? AND ACCEPTANCE >= 0 AND THOUGHTS.SESSION = GROUPING.SESSION
                      ) sessions INNER JOIN POSTS posts ON posts.SESSION = sessions.session ORDER BY posts.TS''',
                   (clustering_id, chosen_group))
    posts = cursor.fetchall()

    # gather votes on posts from sessions that are in the group
    cursor.execute('''SELECT sessions.session, sessions.user, votes.POST_ID, votes.SIGN, votes.TS FROM
                            (SELECT GROUPING.SESSION AS session, THOUGHTS.USER AS user FROM GROUPING, THOUGHTS
                            WHERE CLUSTERING_ID = ? AND GROUP_ID = ? AND ACCEPTANCE >= 0 AND THOUGHTS.SESSION = GROUPING.SESSION
                          ) sessions INNER JOIN VOTES votes ON votes.SESSION = sessions.session''',
                   (clustering_id, chosen_group))
    votes = cursor.fetchall()

    # create a recursive data structure of posts and votes
    # build up level by level
    content = {}
    build_content(content, posts, votes, -1)
    content = OrderedDict(sorted(content.iteritems(), key=lambda post: post[1]['score'], reverse=True))

    # determine if user can give feedback on the current grouping
    cursor = conn.cursor()
    cursor.execute("SELECT ACCEPTANCE FROM GROUPING WHERE SESSION = ? AND CLUSTERING_ID = ? AND GROUP_ID = ?",
                   (session['session'], clustering_id, chosen_group))
    acceptance = cursor.fetchone()

    feedback = None
    if acceptance is not None:
        feedback = acceptance[0]

    return render_template('group.html', sessions=sessions, posts=posts, votes=votes, write_rights=write_rights,
                           message=message, added_vote=added_vote, added_post=added_post, content=content,
                           group=chosen_group, user=session.get('user', None)[:7], clustering_id=clustering_id,
                           feedback=feedback)


def score_post(votes, ts):  # TODO move to another source file
    # count up/down votes
    ups = 0
    downs = 0
    for v in votes:
        if v['sign'] == 1:
            ups += 1
        elif v['sign'] == -1:
            downs += 1

    # from: https://medium.com/hacking-and-gonzo/how-reddit-ranking-algorithms-work-ef111e33d0d9#.l4a2wwi8n
    # mixture of post and comment ranking
    n = ups + downs
    confidence = 1
    if n > 0:
        z = 1.281551565545
        p = float(ups) / n

        left = p + 1 / (2 * n) * z * z
        right = z * np.sqrt(p * (1 - p) / n + z * z / (4 * n * n))
        under = 1 + 1 / n * z * z

        confidence = (left - right) / under

    unix_ts = time.mktime(datetime.strptime(ts, "%Y-%m-%d %H:%M:%S").timetuple())

    score = (ups - downs) * confidence  # multiply score by confidence is experimental
    order = score  # or: np.log10(max(abs(score), 1))
    sign = 1 if score > 0 else -1 if score < 0 else 0
    seconds = np.log10(unix_ts - 1134028003)  # constant keeps the score static as time goes
    return round(sign * order + seconds, 7)  # TODO calculate how many upvotes should correspond to how many time and derive it from there


# builds children of 1 parent and descendants - depth first (anyway, breadth is hard to write)
# post: (session, user, post id, parent, text, timestamp)
# votes: (session, user, post id, sign, timestamp)
def build_content(root, posts, votes, parent):  # TODO move to another source file
    children = [post for post in posts if post[3] == parent]
    posts[:] = [post for post in posts if post[3] != parent]

    for child in children:
        child_id = child[2]
        child_votes = [{'session': vote[0], 'user': vote[1], 'post_id': vote[2], 'sign': vote[3], 'ts': vote[4]}
                       for vote in votes if vote[2] == child_id]
        votes[:] = [vote for vote in votes if vote[2] != child_id]
        root[child_id] = {
            'post': {'session': child[0], 'user': child[1], 'id': child[2], 'text': child[4], 'ts': child[5]},
            'parent': child[3],
            'replies': {},
            'votes': child_votes,
            'vote_count': int(np.sum([v['sign'] for v in child_votes])),
            'score': score_post(child_votes, child[5])
        }
        build_content(root[child_id]['replies'], posts, votes, child_id)

        # order children's replies
        root[child_id]['replies'] = OrderedDict(sorted(root[child_id]['replies'].iteritems(),
                                                key=lambda reply: reply[1]['score'], reverse=True))


@app.route('/groups', methods=['GET'])
def show_groups():

    conn = get_db()
    if 'c' in request.args:
        clustering_id = request.args['c']
    else:
        # get latest clustering id
        clustering_id = sql_commands.latest_clustering_id(conn)

    # get grouping corresponding the latest clustering
    cursor = conn.cursor()
    cursor.execute('''SELECT THOUGHTS.USER AS USER, THOUGHTS.SENTENCE AS SENTENCE, THOUGHTS.SESSION AS SESSION,
                      GROUPING.GROUP_ID AS GROUP_ID FROM THOUGHTS, GROUPING
                      WHERE THOUGHTS.SESSION = GROUPING.SESSION AND GROUPING.CLUSTERING_ID = ?''', (clustering_id,))
    clustered_thougts = cursor.fetchall()

    # show grouping
    grouping = {}
    for t in clustered_thougts:
        group_id = t[3]
        if group_id not in grouping:
            grouping[group_id] = []
        grouping[group_id].append((t[0], t[1], t[2]))

    return '<pre>' + pformat({"clustering": clustering_id, "grouping": grouping}) + '</pre>'


@app.route('/feedback', methods=['GET', 'POST'])
def acceptance_feedback():
    # receive one time feedback and store it
    conn = get_db()
    if 'user' in session and 'session' in session and 'group' in request.form and\
            'feedback' in request.form and request.form['feedback'] in ('1', '-1'):
        session_id = session['session']
        group_id = request.form['group']
        feedback = int(request.form['feedback'])

        cursor = conn.cursor()
        cursor.execute("UPDATE GROUPING SET ACCEPTANCE = ? WHERE SESSION = ? AND GROUP_ID = ? AND ACCEPTANCE = 0",
                       (feedback, session_id, group_id))  # 0 acceptance ensures this is the first and only feedback
        rowcount = cursor.rowcount
        conn.commit()

        if rowcount > 0:
            sql_commands.revive_thought(conn, session['session'])
            return json.dumps({'registered': {'session': session_id, 'group': group_id, 'feedback': feedback}})
        return json.dumps({'error': 'one can only vote once'})
    return json.dumps({'error': 'invalid request, bro'})


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(config.DATABASE)
        if not isfile(config.DATABASE):
            sql_commands.init_db(db)
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


if not isfile(config.DATABASE):
    conn = sqlite3.connect(config.DATABASE)
    sql_commands.init_db(conn)
    conn.close()

if __name__ == "__main__":
    # create database if doesn't exist
    if not isfile(config.DATABASE):
        sql_commands.init_db(sqlite3.connect(config.DATABASE))
    app.run()
