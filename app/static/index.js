thoughtSentFlag = false;

function checkThought(thought) {
    var bulb = document.getElementById("bulb");
    var words = thought.split(" ");
    if (thought.length > 1 && words.length > 1 && words[1].length > 0 && !thoughtSentFlag)
        bulb.disabled = false;
    else
        bulb.disabled = true;
}

$(document).ready(function() {

    setInterval(function() { checkThought($('#thought').val()); }, 200);

    // no click after first click
    $('#bulb').click(function() {
        if (!$(this).prop('disabled')) {
            thoughtSentFlag = true; // disables button in checkThought()
        }
    });
 });
