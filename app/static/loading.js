
// loading animation ;)
var loading_text = null;
var ddot = true;
setInterval(function() {
    if (!loading_text)
        loading_text = document.getElementById('loading_text');

    if (ddot)
        loading_text.innerHTML = loading_text.innerHTML.substring(0, loading_text.innerHTML.length - 1);
    else
        loading_text.innerHTML = loading_text.innerHTML + '.';
    ddot = !ddot;
}, 700);

// check if clustered
var currentClusterId = -1;
var skipped = 0;
var notyet = 0;
setInterval(function() {
    $.get("/clustering/state", function(data) {

        clustered = JSON.parse(data);

        if ("clustered" in clustered) {
            if (clustered.clustered == 1 && "group" in clustered) {
                window.location.replace("/group?group=" + clustered.group);
            }
            else if (clustered.clustered == 1) {
                $("#message").html("hmm.. lost the track of your thought");
            }
            else if (clustered.clustered == -1) {
                // TODO later when still_loading.html is ready
                //window.location.replace("/retry-clustering");
                // for now
                if (currentClusterId > 0 && currentClusterId != clustered.clustering_id) {
                    skipped += 1;
                    $("#message").html(skipped + " skipped");
                }
                currentClusterId = clustered.clustering_id;
            }
            else { // clustered == 0; not yet tried even to cluster
                if (++notyet > 23)
                    $("#message").html("if you are not redirected now, your thought just hasn't matched enough similar" +
                    " ones in the flow; <a href=\"http://thoughtflow.io/\">type in another thought</a>, you can come" +
                    " back to this one later when it is clustered.");
                else if (notyet > 10)
                    $("#message").html("not yet");
            }
        }
    });
}, 2100);
