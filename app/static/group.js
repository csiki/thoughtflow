
function send_feedback(group_id, feedback) {
    $("input#downvote").prop('disabled', true);
    $("input#upvote").prop('disabled', true);

    $.post("/feedback", {"group": group_id, "feedback": feedback}, function(data) {
        if (typeof data.error === "undefined") { // no error
            $("#feedback_zone").html("<p class=\"message\">Vote received</p>");
        }
        else { // error
            $("#feedback_zone").html("<p class=\"message\">" + data.error + "</p>");
        }
    }, "json");
}

$(document).ready(function() {
    $("input#downvote").click(function() {
        send_feedback($("#group_id").attr("value"), "-1")
    });
    $("input#upvote").click(function() {
        send_feedback($("#group_id").attr("value"), "1")
    });
});
