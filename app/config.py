# paths are relative to the root directory (syn)
DATABASE = 'syn.db'
ADJUST_MODEL_PATH = 'adjt_models/'
G2S_MODEL_PATH = 'g2s_models/'
SECRET_KEY = ']R)+G*79jY=V?w[qUg-PP8}7,"<QWvSw}_m@T>rJp+'
SENTENCE_DIM = 4800
GLOVE_DIM = 300
SKIPT_WORD_DIM = 1240  # 2 times 620 (utable and btable)
MIN_THOUGHTS_TO_CLUSTER = 10

MIN_TIME_BETWEEN_POSTS = 5  # in sec
MIN_TIME_BETWEEN_VOTES = 2  # yep
MAX_POST_LEN = 65536
THOUGHT_LIFE_TIME = 1800  # duration of thought existence in the absence of user activity, in seconds

MAX_ADJUSTMENT_NEEDED = 1000  # abs acceptance threshold value above which all grouping

ADJUST_THOUGHT_LEARNING_RATE = 0.001
G2S_LEARNING_RATE = 0.001

SECONDS_BETWEEN_CLUSTERING = 20

CLUSTER_MIN_SAMPLES = 2
