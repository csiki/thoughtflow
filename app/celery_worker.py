from celery import Celery
import sqlite3
import cPickle as pickle
import random
import config
import numpy as np
from lib import clustering, adjust_thought, sql_commands

# flask server
celery = Celery('tasks', broker='amqp://guest@localhost//', backend='amqp://guest@localhost//')


@celery.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(config.SECONDS_BETWEEN_CLUSTERING, cluster.s(), name='cluster')
    # runs adjustment on all freshly accpeted thoughts 5 times (default settings)
    # sender.add_periodic_task(60.0, adjust_dat_thought.s(), name='adjust_thought')
    sender.add_periodic_task(3600.0, clear_dead_weight.s(), name='rm_dead_weight')


@celery.task
def cluster():
    conn = sqlite3.connect(config.DATABASE)
    conn.text_factory = str
    cursor = conn.cursor()
    cursor.execute("SELECT SESSION, USER, SENTENCE, ENCODED FROM THOUGHTS")
    thoughts = cursor.fetchall()
    if len(thoughts) < config.MIN_THOUGHTS_TO_CLUSTER:
        return 'not enough thoughts (%d)' % len(thoughts)

    thought_vectors = np.array([np.reshape(pickle.loads(t[3]), (config.SENTENCE_DIM,)) for t in thoughts])

    min_samples = config.CLUSTER_MIN_SAMPLES
    dist_fun = clustering.cosine_dist
    dist_args = {}
    opt_eps = clustering.find_optimal_clustering(thought_vectors, clustering.dbscan_cluster_cost_fun, dist_fun,
                                                 min_samples, dist_args=dist_args, disp_log=True)

    labels, core_samples = clustering.cluster_thoughts(thought_vectors,
                                                       min_samples, opt_eps, 'precomputed', dist_fun, dist_args)

    # generate a group id for each label that is not -1
    label_id_map = {-1: 0}  # default value for thoughts not clustered (labelled -1) is 0
    individual_labels = set(labels)
    for l in individual_labels:
        if l != -1:
            label_id_map[l] = '%032x' % random.getrandbits(128)

    # fetch latest clustering id and add a new one
    cursor = conn.cursor()
    cursor.execute("INSERT INTO CLUSTERING DEFAULT VALUES")
    clustering_id = cursor.lastrowid

    # compute group mean for each created group
    for l in individual_labels:
        if l != -1:
            grp_mean = np.mean(thought_vectors[labels == l])
            conn.execute("INSERT INTO GROUPS (ID, CLUSTERING_ID, MEAN) VALUES (?, ?, ?)",
                         (label_id_map[l], clustering_id, pickle.dumps(grp_mean)))

    # populate the group assignment table
    for i, t in enumerate(thoughts):
        if labels[i] != -1:
            conn.execute("INSERT INTO GROUPING (SESSION, GROUP_ID, CLUSTERING_ID) VALUES (?, ?, ?)",
                         (t[0], label_id_map[labels[i]], clustering_id))

    # update clustered flag in thoughts table
    for i, t in enumerate(thoughts):
        clustered = 1  # found cluster by default
        if labels[i] == -1:  # clustering failed
            clustered = -1
        conn.execute("UPDATE THOUGHTS SET CLUSTERED = ? WHERE SESSION = ? AND USER = ?", (clustered, t[0], t[1]))

    conn.commit()
    conn.close()

    return 'clustering #%d finished' % clustering_id


# acceptance of group -> contract thought w/ other accepted ones
# rejection of group -> spread thought from other accepted ones
@celery.task
def adjust_dat_thought(n_times=1, acceptance=1, group_id=None, thought_id=None, clustering_id=None):
    if not acceptance:  # acceptance has to be given, and cannot be 0
        return 'no thought adjusted, acceptance is either 0 or not given'

    params = (acceptance, group_id, thought_id, clustering_id)
    columns = ('ACCEPTANCE', 'GROUP_ID', 'SESSION', 'CLUSTERING_ID')
    where_clause = [c + ' = ?' for p, c in zip(params, columns) if p]
    values = [p for p in params if p]

    # contract or retract thoughts depending on acceptance
    # need thought vectors and grouping key to update the adjust model and the grouping table acceptance field
    conn = sqlite3.connect(config.DATABASE)
    cursor = conn.cursor()  # TODO clean query
    mega_query = str("""
                SELECT thought_one.t_session_id, THOUGHTS.SESSION AS other_session, thought_one.t_group_id, thought_one.t_clustering_id,
                 thought_one.t_acceptance, thought_one.t_encoded AS encoded, THOUGHTS.ENCODED AS other_encoded FROM (
                     SELECT thoughts.encoded AS t_encoded, thoughts.group_id AS t_group_id, thoughts.acceptance AS t_acceptance, thoughts.session_id AS t_session_id,
                     thoughts.clustering_id AS t_clustering_id, grouping.SESSION AS other_session_id FROM (
                         SELECT THOUGHTS.ENCODED AS encoded, GROUPING.GROUP_ID AS group_id, GROUPING.ACCEPTANCE AS acceptance,
                         GROUPING.SESSION AS session_id, GROUPING.CLUSTERING_ID AS clustering_id
                         FROM THOUGHTS, GROUPING
                         WHERE GROUPING.GROUP_ID = group_id AND THOUGHTS.SESSION = session_id AND """ + " AND ".join(where_clause) +
                     """) thoughts INNER JOIN GROUPING grouping ON thoughts.clustering_id = grouping.CLUSTERING_ID
                     AND thoughts.group_id = grouping.GROUP_ID WHERE grouping.ACCEPTANCE > 0 AND thoughts.session_id <> grouping.SESSION
                 ) thought_one, THOUGHTS WHERE thought_one.other_session_id = THOUGHTS.SESSION""")

    cursor.execute(mega_query, values)
    training_data = cursor.fetchall()

    # update adjust thought model
    if len(training_data) > 0:
        x_vals = np.array([np.reshape(pickle.loads(str(t[5])), (config.SENTENCE_DIM,)) for t in training_data])
        if acceptance > 0:
            dy_vals = np.array([np.reshape(pickle.loads(str(t[6])), (config.SENTENCE_DIM,)) for t in training_data])
            adjust_thought.train_adjt(config.ADJUST_MODEL_PATH, x_vals=x_vals, dy_vals=dy_vals, n_times=n_times)
        else:  # acceptance < 0
            notdy_vals = np.array([np.reshape(pickle.loads(str(t[6])), (config.SENTENCE_DIM,)) for t in training_data])
            # dy = x + (x - notdy) = 2x - notdy
            dy_vals = 2 * x_vals - notdy_vals
            adjust_thought.train_adjt(config.ADJUST_MODEL_PATH, x_vals=x_vals, dy_vals=dy_vals, n_times=n_times)

        # update ACCEPTANCE in grouping ~ add n_times to the abs value
        # build list of tuples (session_id, clustering_id, group_id)
        # compute unique on them and update database accordingly
        new_acceptance = acceptance + (n_times if acceptance > 0 else -n_times)
        groupings_updated = list(set([(t[0], t[3], t[2]) for t in training_data]))
        for grp in groupings_updated:
            conn.execute("UPDATE GROUPING SET ACCEPTANCE = ? WHERE SESSION = ? AND CLUSTERING_ID = ? AND GROUP_ID = ?",
                         (new_acceptance,) + grp)

    conn.commit()
    conn.close()
    return 'adjust thought model updated by %d samples, %d times' % (len(training_data), n_times)


# @celery.task
# def rm_dead_thoughts():
#     conn = sqlite3.connect(config.DATABASE)
#     conn.execute("DELETE FROM THOUGHTS WHERE (JULIANDAY(CURRENT_TIMESTAMP) - JULIANDAY(ALIVE)) * 86400.0 > ?",
#                  (config.THOUGHT_LIFE_TIME,))
#     conn.commit()


# clear all database records corresponding to now dead thoughts, or old clustering
def clear_dead_weight():
    conn = sqlite3.connect(config.DATABASE)

    # rm grouping records with old clustering ids
    clustering_id = sql_commands.latest_clustering_id(conn)
    conn.execute("DELETE FROM GROUPING WHERE CLUSTERING_ID < ? AND ABS(ACCEPTANCE) > ?",
                 (clustering_id, config.MAX_ADJUSTMENT_NEEDED))

    # find dead thoughts - it's ok to kill them, learn from the feedback until they are alive
    cursor = conn.cursor()
    cursor.execute("SELECT SESSION FROM THOUGHTS WHERE (JULIANDAY(CURRENT_TIMESTAMP) - JULIANDAY(ALIVE)) * 86400.0 > ?",
                   (config.THOUGHT_LIFE_TIME,))
    thoughts = cursor.fetchall()

    # delete dead thoughts
    conn.execute("DELETE FROM THOUGHTS WHERE (JULIANDAY(CURRENT_TIMESTAMP) - JULIANDAY(ALIVE)) * 86400.0 > ?",
                 (config.THOUGHT_LIFE_TIME,))

    # delete all content corresponding to dead thoughts
    for thought in thoughts:
        conn.execute("DELETE FROM POSTS WHERE SESSION = ?", (thought,))
        conn.execute("DELETE FROM VOTES WHERE SESSION = ?", (thought,))

    conn.commit()


# if __name__ == '__main__':
#     pass
