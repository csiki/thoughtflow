import tensorflow as tf
from keras import backend as K
from keras.layers import Dense
from keras.objectives import mean_squared_error
from keras.metrics import categorical_accuracy as accuracy
import numpy as np
import matplotlib.pyplot as plt

sess = tf.Session()
K.set_session(sess)

# TODO give a better name then adjusting_network
learning_rate = 0.5  # change according to user or global level network adjustments
sent_dim = 4800

x = tf.placeholder(tf.float32, shape=(None, sent_dim))
dy = tf.placeholder(tf.float32, shape=(None, sent_dim))
# input: tanh(x), output: x, fun to approx: arctanh
# this works best with input values [-1,1]

wh = np.identity(sent_dim)
wo = np.identity(sent_dim)
bh = np.zeros(sent_dim)
bo = np.zeros(sent_dim)
h_dense = Dense(sent_dim, activation='tanh', weights=(wh, bh), bias=True)
y_dense = Dense(sent_dim, activation='tanh', weights=(wo, bo), bias=True)
h = h_dense(x)
y = y_dense(h)

arctanh_fun = lambda u: 0.5 * tf.log((1.0 + u) / (1.0 - u))
final_y = tf.map_fn(arctanh_fun, tf.map_fn(arctanh_fun, y))

# train
loss = tf.reduce_mean(mean_squared_error(dy, final_y))
train_step = tf.train.GradientDescentOptimizer(learning_rate=0.5).minimize(loss)

# pre init x_val and dy_val to test if it's able to learn the new representation
x_val = np.random.uniform(-1.0, 1.0, size=(100, sent_dim))
change_in_x = np.random.uniform(-0.3, 0.3, size=(100, sent_dim))
dy_val = x_val + change_in_x

with sess.as_default():

    for i in range(200):
        # plt.matshow(h_dense.W[:].eval())
        # x_val = np.random.uniform(-1.0, 1.0, size=(100, 4800))
        # dy_val = x_val.copy()
        _, h_val, y_val, h_W, h_b, y_W, y_b, final_y_val, loss_val = sess.run([train_step, h, y, h_dense.W, h_dense.b, y_dense.W, y_dense.b, final_y, loss], feed_dict={x: x_val, dy: dy_val})
        # print loss_val
        # plt.matshow([x_val[1][:10], dy_val[1][:10], final_y_val[1][:10]])
        print x_val[0][0], dy_val[0][0], final_y_val[0][0]
    print np.sum(y_b), np.sum(h_b), np.sum(h_W), np.sum(y_W)
    # plt.matshow(y_W)

# test
acc_value = accuracy(dy, final_y)
with sess.as_default():
    # x_val = np.random.uniform(-1.0, 1.0, size=(100, 4800))
    # dy_val = x_val.copy()
    print "accuracy:", acc_value.eval(feed_dict={x: x_val, dy: dy_val})

plt.show()
