#!/usr/bin/env python2

import requests
from urllib import urlencode

FILE = "test_thoughts.txt"
URL = "http://localhost:8080/syn"

f = open(FILE)
d = f.read()
f.close()

thoughts = [(line.split()[0], " ".join(line.split()[1:])) for line in d.split("\n")[:-1]]
for thought in thoughts:
  payload = urlencode(dict(user=thought[0], thought=thought[1]))
  print(payload)
  r = requests.post(URL, data=payload)
  if r.status_code != 200:
    print("error: ", r.text)
  print(r.text)
