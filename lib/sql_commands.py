
def revive_thought(conn, session):
    conn.execute("UPDATE THOUGHTS SET TS = CURRENT_TIMESTAMP WHERE SESSION = ?", (session,))
    conn.commit()


def latest_clustering_id(conn):
    cursor = conn.cursor()
    cursor.execute("SELECT MAX(ID) FROM CLUSTERING")
    latest = cursor.fetchone()
    if len(latest) > 0:
        return latest[0]
    return -1


def init_db(conn):
    # live thoughts table - 1 thought, 1 session
    conn.execute('''CREATE TABLE THOUGHTS(
        SESSION     CHAR(32) PRIMARY KEY NOT NULL,
        USER        CHAR(32) NOT NULL,
        SENTENCE    TEXT NOT NULL,
        ENCODED     BLOB NOT NULL,
        TS          DATETIME DEFAULT CURRENT_TIMESTAMP,
        ALIVE       DATETIME DEFAULT CURRENT_TIMESTAMP,
        CLUSTERED   INTEGER(1) NOT NULL);''')
    # clustered: 0: not yet clustered; 1: clustering succeeded, found a group; -1: clustering failed = no group found
    # alive: timestamp of the last interaction - like/comment/vote

    # table to save group assignment and feedback
    conn.execute('''CREATE TABLE GROUPING(
        SESSION         CHAR(32) NOT NULL,
        GROUP_ID        CHAR(32) NOT NULL,
        CLUSTERING_ID   INTEGER NOT NULL,
        ACCEPTANCE      INTEGER NOT NULL DEFAULT 0,
        TS              DATETIME DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (SESSION, GROUP_ID, CLUSTERING_ID));''')
    # acceptance: 0: feedback on grouping not yet given; >0: accepted grouping; <0: rejected grouping
    # |n| - 1 is the number of time the acceptance feedback has been passed through adjust thought

    # table to store clustering ids w/ their date of creation
    conn.execute('''CREATE TABLE CLUSTERING(
        ID     INTEGER PRIMARY KEY AUTOINCREMENT,
        TS     DATETIME DEFAULT CURRENT_TIMESTAMP);''')

    # table to store posts and comments
    conn.execute('''CREATE TABLE POSTS(
        ID      INTEGER PRIMARY KEY AUTOINCREMENT,
        SESSION CHAR(32),
        PARENT  INTEGER DEFAULT -1,
        TEXT    TEXT NOT NULL,
        TS      DATETIME DEFAULT CURRENT_TIMESTAMP);''')

    # table to store (positive and negative) votes
    conn.execute('''CREATE TABLE VOTES(
        SESSION CHAR(32),
        POST_ID INTEGER NOT NULL,
        SIGN    INTEGER(1) NOT NULL,
        TS      DATETIME DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (SESSION, POST_ID));''')
    # sign could be 1 or -1, up and down vote, respectively

    # table to store (temporary) groups
    conn.execute('''CREATE TABLE GROUPS(
        ID              CHAR(32) PRIMARY KEY NOT NULL,
        CLUSTERING_ID   INTEGER NOT NULL,
        MEAN            BLOB NOT NULL,
        TS              DATETIME DEFAULT CURRENT_TIMESTAMP);''')
    # mean just to compute fast the nearby groups, but not to use in learning the acceptance feedback

    # table to store (initially) unknown words
    conn.execute('''CREATE TABLE UNKNOWN_WORDS(
        WORD                CHAR(200) PRIMARY KEY,
        MINE                TEXT DEFAULT '',
        EXAMPLES_PASSED     INTEGER DEFAULT 0,
        TS                  DATETIME DEFAULT CURRENT_TIMESTAMP);''')
    # mine contains sentences (and maybe surrounding sentences) that contains the unknown word
    # examples passed number of sentences (or sentences with neighboring sentences) passed through the word2vec training

    conn.commit()
