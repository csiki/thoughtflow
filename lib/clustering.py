import numpy as np
from sklearn.cluster import DBSCAN
from sklearn import metrics
from scipy import optimize


def jaccard_dist(v1, v2):
    v1_shifted = v1 - np.min(v1)
    v2_shifted = v2 - np.min(v2)
    return 1.0 - np.sum(np.min([v1_shifted, v2_shifted], axis=0)) / np.sum(np.max([v1_shifted, v2_shifted], axis=0))


def cosine_dist(v1, v2):
    # 1 - (cos_sim(v1,v2) + 1) / 2, as cos_sim ranges [-1,1], and we need reversed (distance = dissimilarity) [0,1]
    cos_sim = np.sum(np.multiply(v1, v2)) / (np.sqrt(np.sum(np.multiply(v1, v1))) * np.sqrt(np.sum(np.multiply(v2, v2))))
    return 1.0 - (cos_sim + 1) / 2.0


def lpnorm_dist(v1, v2, p):
    return np.sum(np.power(np.abs(v1 - v2), np.repeat(p, len(v1))))


def dist_mx(X, dist_fun, dist_args={}):
    D = np.zeros((len(X), len(X)))
    for i1, x1 in enumerate(X):
        for i2, x2 in enumerate(X):
            D[i1, i2] = dist_fun(x1, x2, **dist_args)
    return D


def cluster_thoughts(thoughts, min_samples, eps, metric_type, dist_fun, dist_args):
    if metric_type == 'precomputed':
        dbscan_input = dist_mx(thoughts, dist_fun, dist_args)
    else:
        dbscan_input = thoughts

    db = DBSCAN(eps=eps, min_samples=min_samples, metric=metric_type).fit(dbscan_input)
    return db.labels_, db.core_sample_indices_


def dbscan_cluster_cost_fun(eps, *args):
    if eps[0] < 0 or eps[0] > 1:  # eps can only be in the range (0,1)
        return 1

    X, D, metric, min_samples = args
    if metric == 'precomputed':
        dbscan_input = D
    else:
        dbscan_input = X

    db = DBSCAN(eps=eps[0], min_samples=min_samples, metric=metric).fit(dbscan_input)
    if len(set(db.labels_)) < 2:  # at least 2 clusters are needed
        return 1

    # returns [0,1], where 0 is the best, 1 is the worst
    # silhouette outputs [-1,1] -> [0,1] and inverted
    return 1 - (metrics.silhouette_score(X, db.labels_) + 1) / 2.0


def find_optimal_clustering(thoughts, cost_fun, dist_fun, min_samples, metric='precomputed', dist_args={}, eps0=0.2, disp_log=False, niter=500):
    D = None  # distance mx
    if metric == 'precomputed':  # metric can be a string - 'precomputed' - or a callable metric function
        D = dist_mx(thoughts, dist_fun, dist_args)
    params = {'args': (thoughts, D, metric, min_samples)}
    x0 = np.array([eps0])  # initial eps value - has to be a numpy array
    res = optimize.basinhopping(cost_fun, x0=x0, minimizer_kwargs=params, disp=disp_log, niter=niter)
    return res.x
