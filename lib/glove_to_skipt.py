import tensorflow as tf
from keras.objectives import mean_squared_error
import numpy as np
import spacy
from adjust_thought import does_file_exist_in_dir
import sys
from os.path import join

# to import module from parent directory
import os
import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
os.sys.path.insert(0, parentdir)
from app import config
import skip_thoughts.skipthoughts


def glove2skipt(word):
    model = init_g2s()
    saver = tf.train.Saver()
    init_op = tf.global_variables_initializer()
    with tf.Session() as sess:
        sess.run(init_op)
        checkpoint = tf.train.latest_checkpoint(join('..', config.G2S_MODEL_PATH))
        print checkpoint
        saver.restore(sess, checkpoint)
        return sess.run(model['y'], feed_dict={model['x']: word})


def init_g2s(learning_rate=config.G2S_LEARNING_RATE):
    x = tf.placeholder(tf.float32, shape=(None, config.GLOVE_DIM), name='x')
    dy = tf.placeholder(tf.float32, shape=(None, config.SKIPT_WORD_DIM), name='dy')
    W = tf.Variable(tf.truncated_normal((config.GLOVE_DIM, config.SKIPT_WORD_DIM)), name='W')  # translation matrix
    y = tf.matmul(x, W, name='y')

    global_step = tf.Variable(0, name='global_step', trainable=False)
    loss = tf.reduce_mean(mean_squared_error(dy, y), name='loss')
    train_step = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss, global_step=global_step, name='train_step')

    return {'x': x, 'y': y, 'dy': dy, 'W': W, 'global_step': global_step, 'train': train_step,
            'loss': loss, 'vars': [W, global_step]}


def train_g2s(path, model_name='model', batch_size=20, n_iter=10000):

    model = init_g2s()
    saver = tf.train.Saver()

    # TODO apply this raw way of training to adjust thought, then update model runs in app.py, skipthoughts.py, celery_worker.py

    glove = spacy.load('en')
    skipthought = skip_thoughts.skipthoughts.load_model()

    # prepare glove dictionary
    Vg = np.array([glove.vocab[wg_i].vector for wg_i in xrange(len(glove.vocab))])
    Vgstr = [glove.vocab[wg_i].orth_ for wg_i in xrange(len(glove.vocab))]

    # prepare skipt words corresponding to the glove dictionary
    Vsu = skip_thoughts.skipthoughts.word_features(skipthought['utable'])
    Vsb = skip_thoughts.skipthoughts.word_features(skipthought['btable'])

    # generate mapping from skipt (str) words to their index
    s_index_mapping = {}
    s_str = skipthought['utable'].keys()
    for i in range(len(skipthought['utable'])):
        s_index_mapping[s_str[i]] = i

    i = 0
    g_indices = []
    s_indices = []
    for wg_str in Vgstr:
        if wg_str in s_index_mapping:
            g_indices.append(i)
            s_indices.append(s_index_mapping[wg_str])
            i += 1

    g_indices = np.array(g_indices)
    Vg = Vg[g_indices]

    s_indices = np.array(s_indices)
    Vs = np.c_[Vsu[s_indices], Vsb[s_indices]]

    # run training
    init_op = tf.global_variables_initializer()
    with tf.Session() as sess:
        sess.run(init_op)

        if does_file_exist_in_dir(path):
            checkpoint = tf.train.latest_checkpoint(path)
            print 'model restored from ' + checkpoint
            saver.restore(sess, checkpoint)
        else:
            print 'brand new model initiated'

        for i in xrange(n_iter):
            batch_indices = np.random.permutation(len(Vg))[:batch_size]
            Vg_batch = Vg[batch_indices]
            Vs_batch = Vs[batch_indices]
            _, loss_val = sess.run([model['train'], model['loss']],
                                   feed_dict={model['x']: Vg_batch, model['dy']: Vs_batch})

            if i % 1000 == 0:
                print '#%d - loss: %f' % (i, loss_val)

        new_path = saver.save(sess, join(path, model_name), global_step=model['global_step'])
        print 'trained model is saved to ' + new_path


if __name__ == '__main__':
    n_iter = 10000
    if len(sys.argv) > 1:
        n_iter = int(sys.argv[1])
    batch_size = 20
    if len(sys.argv) > 2:
        batch_size = int(sys.argv[2])

    print 'g2s training starts with a batch size of %d and %d iterations' % (batch_size, n_iter)

    path = join('..', config.G2S_MODEL_PATH)
    print 'check model at ' + path
    train_g2s(path, batch_size=batch_size, n_iter=n_iter)
    print 'cool'
