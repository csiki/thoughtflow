import tensorflow as tf
from keras.layers import Dense
from keras.objectives import mean_squared_error
import numpy as np
from os import listdir
from os.path import join, isfile

# FIXME DEPRECATED - this sort of adjustment cannot work, and possibly the feedback gained cannot be used

# to import module from parent directory
import os
import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
os.sys.path.insert(0, parentdir)
from app import config


def does_file_exist_in_dir(path):
    try:
        return any(isfile(join(path, i)) for i in listdir(path))
    except:
        return False


def adjust(thought):
    model = init_adjt()
    saver = tf.train.Saver()
    init_op = tf.global_variables_initializer()
    with tf.Session() as sess:
        sess.run(init_op)
        checkpoint = tf.train.latest_checkpoint(join('..', config.ADJUST_MODEL_PATH))
        saver.restore(sess, checkpoint)
        return sess.run(model['y'], feed_dict={model['x']: thought})


def init_adjt(learning_rate=config.ADJUST_THOUGHT_LEARNING_RATE):
    x = tf.placeholder(tf.float32, shape=(None, config.SENTENCE_DIM), name='x')
    dy = tf.placeholder(tf.float32, shape=(None, config.SENTENCE_DIM), name='dy')
    # input: tanh(x), output: x, fun to approx: arctanh
    # this works best with input values [-1,1] - and the sentence vector values should be in that region

    wh = np.identity(config.SENTENCE_DIM)
    wo = np.identity(config.SENTENCE_DIM)
    bh = np.zeros(config.SENTENCE_DIM)
    bo = np.zeros(config.SENTENCE_DIM)
    h_dense = Dense(config.SENTENCE_DIM, activation='tanh', weights=(wh, bh), bias=True, name='h')
    y_dense = Dense(config.SENTENCE_DIM, activation='tanh', weights=(wo, bo), bias=True, name='y')
    h = h_dense(x)
    final_y = y_dense(h)

    #arctanh_fun = lambda u: 0.5 * tf.log((1.0 + u) / (1.0 - u))
    #final_y = tf.map_fn(arctanh_fun, tf.map_fn(arctanh_fun, y), name='final_y')

    global_step = tf.Variable(0, name='global_step', trainable=False)
    loss = tf.reduce_mean(mean_squared_error(dy, final_y))
    train_step = tf.train.GradientDescentOptimizer(learning_rate=learning_rate).minimize(loss, global_step=global_step, name='train')

    return {'x': x, 'y': final_y, 'dy': dy, 'train': train_step, 'global_step': global_step, 'vars': [], 'h': h}  # TODO save variables !!!!!! get variables out of keras


def train_adjt(path, x_vals, dy_vals, model_name='model', n_times=1):

    model = init_adjt()
    saver = tf.train.Saver(tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES))

    init_op = tf.global_variables_initializer()
    with tf.Session() as sess:
        sess.run(init_op)

        if does_file_exist_in_dir(path):
            checkpoint = tf.train.latest_checkpoint(path)
            print 'model restored from ' + checkpoint
            saver.restore(sess, checkpoint)
        else:
            print 'brand new model initiated'

        for i in xrange(n_times):
            vals_perm = np.random.permutation(np.arange(len(x_vals)))
            _ = model['session'].run(model['train'],
                                     feed_dict={model['x']: x_vals[vals_perm], model['dy']: dy_vals[vals_perm]})

        new_path = saver.save(sess, join(path, model_name), global_step=model['global_step'])
        print 'trained model is saved to ' + new_path


if __name__ == '__main__':
    path = join('..', config.ADJUST_MODEL_PATH)
    if not does_file_exist_in_dir(path):
        # initiate adjust thought model with basic parameters
        model = init_adjt()
        saver = tf.train.Saver(tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES))
        init_op = tf.global_variables_initializer()
        with tf.Session() as sess:
            sess.run(init_op)


            x_val = np.reshape(np.linspace(0, 0.1, 4800), (1, -1))
            y_val, h_val = sess.run([model['y'], model['h']], feed_dict={model['x']: x_val})
            print x_val
            print h_val
            print y_val
            print x_val == y_val


            new_path = saver.save(sess, join(path, 'model'), global_step=model['global_step'])
            print 'adjust thought model is created and stored at ' + new_path
    else:
        print 'model is already initiated'
        model = init_adjt()
        saver = tf.train.Saver(tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES))
        init_op = tf.global_variables_initializer()
        with tf.Session() as sess:
            sess.run(init_op)
            checkpoint = tf.train.latest_checkpoint(path)
            print 'model restored from ' + checkpoint


            for i in xrange(1000):
                x_val = np.random.random((100, 4800))
                sess.run(model['train'], feed_dict={model['x']: x_val, model['dy']: x_val})


            #saver.restore(sess, checkpoint)
            x_val = np.reshape(np.linspace(0, 0.1, 4800), (1, -1))
            y_val, h_val = sess.run([model['y'], model['h']], feed_dict={model['x']: x_val})

            print x_val
            print h_val
            print y_val
            print x_val == y_val

            #print 'weight', sess.run()
